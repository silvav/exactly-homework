all: up composer migrate seed check-style

reset-data: migrate seed

test: up phpunit

up :
	docker-compose up -d

composer :
	docker exec -it exactly_app /bin/bash -c "cd /var/www/src/ && composer install"

migrate :
	docker exec -it exactly_app /bin/bash -c "cd /var/www/src/ && php artisan migrate:refresh"

seed :
	docker exec -it exactly_app /bin/bash -c "cd /var/www/src/ && php artisan db:seed"

check-style :
	docker exec -it exactly_app /bin/bash -c "cd /var/www/src/ && vendor/bin/ecs check . --config ecs.yaml"

down :
	docker-compose down

rebuild :
	docker-compose up -d --no-deps --build

phpunit :
	docker exec -it exactly_app /bin/bash -c "cd /var/www/src/ && ./vendor/bin/phpunit -c phpunit.xml ./tests"

charge-subscriptions :
	docker exec -it exactly_app /bin/bash -c "cd /var/www/src/ && php artisan charge:subscriptions"

package : down
	docker rmi php:7.4-fpm mysql:5.7.22 nginx:alpine && rm -r ./src/vendor/*
