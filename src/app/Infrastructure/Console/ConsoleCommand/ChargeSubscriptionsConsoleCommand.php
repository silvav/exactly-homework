<?php

declare(strict_types=1);

namespace App\Infrastructure\Console\ConsoleCommand;

use App\Application\Command\ChargeSubscriptions;
use Illuminate\Console\Command;

final class ChargeSubscriptionsConsoleCommand extends Command
{
    protected $signature = 'charge:subscriptions';

    protected $description = 'Subscription fees charge collection';

    public function handle(): void
    {
        ChargeSubscriptions::dispatchNow();
    }
}
