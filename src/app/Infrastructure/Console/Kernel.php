<?php

namespace App\Infrastructure\Console;

use App\Infrastructure\Console\ConsoleCommand\ChargeSubscriptionsConsoleCommand;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

final class Kernel extends ConsoleKernel
{
    protected $commands = [
        ChargeSubscriptionsConsoleCommand::class,
    ];

    protected function scheduleTimezone()
    {
        return 'UTC';
    }

    protected function schedule(Schedule $schedule)
    {
        // When running multiple instances should be run in only one server
        $schedule->command('charge:subscriptions')
            ->at('02:00')
            ->runInBackground();
    }
}
