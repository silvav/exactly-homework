<?php

declare(strict_types=1);

namespace App\Infrastructure\Persistence\Mysql;

use App\Domain\Entity\Account;
use App\Domain\Repository\AccountRepository as AccountRepositoryInterface;

final class AccountRepository implements AccountRepositoryInterface
{
    public function getByIdWithLock($id, $withLock = false): Account
    {
        return Account::where('id', $id)->lockForUpdate()->first();
    }

    public function get(int $id): Account
    {
        return Account::findOrFail($id);
    }
}
