<?php

declare(strict_types=1);

namespace App\Infrastructure\Persistence\Mysql;

use App\Domain\Entity\AccountTransaction;
use App\Domain\Repository\AccountTransactionRepository as AccountTransactionRepositoryInterface;

final class AccountTransactionRepository implements AccountTransactionRepositoryInterface
{
    public function findByAccountId(int $accountId)
    {
        return AccountTransaction::where('account_id', $accountId)->get();
    }

    public function bulkInsert(array $accountTransactions)
    {
        AccountTransaction::insert(
            array_map(
                function ($item) {
                    $data = $item->toArray();
                    $now = new \DateTime();
                    $data['created_at'] = $now;
                    $data['updated_at'] = $now;
                    return $data;
                },
                $accountTransactions
            )
        );
    }
}
