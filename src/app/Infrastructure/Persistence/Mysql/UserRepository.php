<?php

declare(strict_types=1);

namespace App\Infrastructure\Persistence\Mysql;

use App\Domain\Entity\User;
use App\Domain\Exceptions\DuplicateUserException;
use App\Domain\Repository\UserRepository as UserRepositoryInterface;
use Illuminate\Database\QueryException;

final class UserRepository implements UserRepositoryInterface
{
    private const INTEGRITY_CONSTRAINT_VIOLATION = 23000;

    public function get(int $id): User
    {
        return User::where('id', $id)->first();
    }

    /**
     * @throws DuplicateUserException
     */
    public function save(User $user): void
    {
        try {
            $user->save();
        } catch (QueryException $exception) {
            if ((int) $exception->getCode() === self::INTEGRITY_CONSTRAINT_VIOLATION) {
                throw new DuplicateUserException('Unique violation');
            }
            throw $exception;
        }
    }
}
