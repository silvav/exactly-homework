<?php

declare(strict_types=1);

namespace App\Infrastructure\Persistence\Mysql;

use App\Domain\Entity\Subscription;
use App\Domain\Repository\SubscriptionRepository as SubscriptionRepositoryInterface;
use DateTimeImmutable;

final class SubscriptionRepository implements SubscriptionRepositoryInterface
{
    /**
     * {@inheritDoc}
     */
    public function findChargeable(): iterable
    {
        return Subscription::where('charge_at', '<=', new DateTimeImmutable())->get();
    }

    public function findAll()
    {
        return Subscription::all();
    }

    public function get(int $id): Subscription
    {
        return Subscription::findOrFail($id);
    }
}
