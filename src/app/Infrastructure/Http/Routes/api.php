<?php

declare(strict_types=1);

use Dingo\Api\Routing\Router;

/** @var Router $api */
$api = app(Router::class);

$api->version(
    'v1',
    function (Router $api) {
        $api->group(
            ['prefix' => 'users/{id?}'],
            function (Router $api, $id = null) {
                $api->post('/', 'App\\Infrastructure\\Http\\Controller\\UserController@register');
                $api->post('/login', 'App\\Infrastructure\\Http\\Controller\\UserController@login');

                $api->group(
                    ['middleware' => 'jwt.auth'],
                    function (Router $api) {
                        $api->get('/', 'App\\Infrastructure\\Http\\Controller\\UserController@getUser');
                    }
                );
            }
        );

        $api->group(
            [
                'prefix' => 'users/{user_id}/accounts/{account_id}',
                'middleware' => 'jwt.auth',
            ],
            function (Router $api) {
                $api->post(
                    'transfer',
                    'App\\Infrastructure\\Http\\Controller\\AccountController@transfer'
                );
            }
        );

        $api->group(
            [
                'prefix' => 'users/{user_id}/accounts/{account_id}/transactions',
                'middleware' => 'jwt.auth',
            ],
            function (Router $api) {
                $api->get(
                    '/',
                    'App\\Infrastructure\\Http\\Controller\\AccountTransactionController@getTransactions'
                );
            }
        );
    }
);
