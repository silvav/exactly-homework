<?php

use Illuminate\Support\Facades\Config;

Route::get('/', static function () {
    return Config::get('app.name');
});
