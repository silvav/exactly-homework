<?php

declare(strict_types=1);

namespace App\Infrastructure\Http\Controller;

use App\Application\Command\RegisterUser;
use App\Domain\Entity\User;
use App\Domain\Exceptions\DuplicateUserException;
use App\Infrastructure\Http\Request\LoginRequest;
use App\Infrastructure\Http\Request\RegisterRequest;
use App\Infrastructure\Http\Resource\UserResource;
use Auth;
use Dingo\Blueprint\Annotation;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Throwable;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\JWTAuth;

/**
 * User resource representation.
 *
 * @Annotation\Resource("Users", uri="/users")
 */
final class UserController extends Controller
{
    /**
     * Register an User
     *
     * @Annotation\Method\Post("/")
     * @Annotation\Versions({"v1"})
     * @Annotation\Request({"name": "foo", "email": "foo@bar.com", "password": "secret"})
     * @Annotation\Response(201, body={"data": {"id": 1, "name": "foo", "email":"foo@bar.com"}})
     * @Annotation\Response(409, body={"data": {"id": 1, "name": "foo", "email":"foo@bar.com"}})
     * @Annotation\Response(
     *     422,
     *     body={
     *          "error": {
     *                  "message": "422 Unprocessable Entity",
     *                  "errors": {"password": ["name field is required."]
     *          },
     *          "status_code": 422
     *     }})
     */
    public function register(RegisterRequest $request, JWTAuth $JWTAuth)
    {
        try {
            $user = new User($request->all());
            RegisterUser::dispatchNow($user);
        } catch (DuplicateUserException $exception) {
            throw new HttpException(Response::HTTP_CONFLICT);
        } catch (Throwable $exception) {
            throw new HttpException(Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        return response()->json(
            new UserResource($user),
            Response::HTTP_CREATED
        );
    }

    /**
     * Logs in an User
     *
     * @Annotation\Method\Post("{user_id}/login")
     * @Annotation\Versions({"v1"})
     * @Annotation\Request({"email": "foo@bar.com", "password": "secret"})
     * @Annotation\Response(200, body={"data": {"id": 1, "name": "foo", "email":"foo@bar.com"}})
     * @Annotation\Response(403, body={"data": {"id": 1, "name": "foo", "email":"foo@bar.com"}})
     */
    public function login(int $userId, LoginRequest $request, JWTAuth $JWTAuth)
    {
        $credentials = $request->only(['email', 'password']);

        try {
            $token = Auth::guard()->attempt($credentials);
            $user = Auth::user();

            if (!$token || $userId !== $user->getKey()) {
                throw new AccessDeniedHttpException();
            }
        } catch (JWTException $exception) {
            throw new HttpException(500);
        }

        $guard = Auth::guard()->factory();
        $expiresIn = $guard->getTTL() * 60;

        return new UserResource($user, [
            'token' => $token,
            'expires_in' => $expiresIn
        ]);
    }

    /**
     * Gets user info
     *
     * @Annotation\Method\Get("/{user_id}")
     * @Annotation\Versions({"v1"})
     * @Annotation\Response(200, body={"data": {
     *     "id": 1,
     *     "name": "foo",
     *     "email":"foo@bar.com",
     *     "account": {
     *          "id": 5,
     *          "balance": 0,
     *          "created_at": "2020-04-06 00:28:00"
     *     }
     * }})
     * @Annotation\Response(200, body={"data": {"id": 1, "name": "foo", "email":"foo@bar.com"}})
     * @Annotation\Response(403, body={"error": {"message": "403 Forbidden", "status_code": 403}})
     */
    public function getUser(int $userId, JWTAuth $JWTAuth)
    {
        $user = Auth::user();
        if ($user->getKey() !== $userId) {
            throw new AccessDeniedHttpException();
        }

        return new UserResource($user);
    }
}
