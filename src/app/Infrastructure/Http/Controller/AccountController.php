<?php

declare(strict_types=1);

namespace App\Infrastructure\Http\Controller;

use App\Application\Command\TransferFunds;
use App\Domain\Entity\User;
use App\Domain\Exceptions\InsufficientAccountFunds;
use App\Infrastructure\Http\Request\TransferRequest;
use App\Infrastructure\Http\Resource\AccountResource;
use Auth;
use Dingo\Blueprint\Annotation;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Throwable;

/**
 * Account resource representation.
 *
 * @Annotation\Resource("Accounts", uri="/users/{user_id}/accounts/{account_id}")
 */
final class AccountController extends Controller
{
    private LoggerInterface $logger;

    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    /**
     * Transfers Account funds to another Account(s)
     *
     * @Annotation\Method\Post("/transfer")
     * @Annotation\Versions({"v1"})
     * @Annotation\Response(200, body={"status": "ok"})
     * @Annotation\Response(401, body={"error": {"message": "Token not provided", "status_code": 401}})
     */
    public function transfer(TransferRequest $request, int $userId)
    {
        /** @var User $user */
        $user = Auth::user();

        $transfers = $request->post('transfers');
        $transfers = array_map(static function ($transfer) {
            $transfer['amount'] = (int) ($transfer['amount'] * 100);
            return $transfer;
        }, $transfers);

        try {
            $account = $user->getAccount();
            TransferFunds::dispatchNow($account, $transfers);
        } catch (InsufficientAccountFunds $exception) {
            $this->logger->warning($exception->getMessage(), ['exception' => $exception]);
            return response()->json(
                ['error' => $exception->getMessage()],
                Response::HTTP_UNPROCESSABLE_ENTITY
            );
        } catch (Throwable $exception) {
            $this->logger->error($exception->getMessage(), ['exception' => $exception]);
            throw new HttpException(500);
        }

        return new AccountResource($account->refresh());
    }
}
