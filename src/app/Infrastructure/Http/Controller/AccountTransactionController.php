<?php

declare(strict_types=1);

namespace App\Infrastructure\Http\Controller;

use App\Domain\Entity\AccountTransaction;
use App\Infrastructure\Http\Resource\AccountTransactionCollection;
use Auth;
use Dingo\Blueprint\Annotation;
use Tymon\JWTAuth\JWTAuth;

/**
 * Account Transaction resource representation.
 *
 * @Annotation\Resource("AccountTransactions", uri="/users/{user_id}/accounts/{account_id}/transactions")
 */
final class AccountTransactionController extends Controller
{
    /**
     * Retrieves Account Transactions
     *
     * @Annotation\Method\Get("/")
     * @Annotation\Versions({"v1"})
     * @Annotation\Response(200)
     */
    public function getTransactions(int $userId, int $accountId, JWTAuth $JWTAuth)
    {
        $transactions = AccountTransaction::where('account_id', $accountId)->orderBy('created_at', 'desc')->paginate(5);

        return new AccountTransactionCollection($transactions);
    }
}
