<?php

declare(strict_types=1);

namespace App\Infrastructure\Http\Request;

use Dingo\Api\Http\FormRequest;

final class TransferRequest extends FormRequest
{
    public function rules()
    {
        return [
            'transfers' => 'required',
            'transfers.*.account_id' => 'required|integer|exists:accounts,id',
            'transfers.*.amount' => 'required|regex:/^\d+(\.\d{2})?$/',
        ];
    }

    public function authorize()
    {
        return true;
    }
}
