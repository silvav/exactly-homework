<?php

declare(strict_types=1);

namespace App\Infrastructure\Http\Request;

use Dingo\Api\Http\FormRequest;

final class RegisterRequest extends FormRequest
{
    public function rules()
    {
        return [
            'name' => 'required',
            'email' => 'required|email',
            'password' => 'required'
        ];
    }

    public function authorize()
    {
        return true;
    }
}
