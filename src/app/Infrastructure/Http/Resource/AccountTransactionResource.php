<?php

declare(strict_types=1);

namespace App\Infrastructure\Http\Resource;

use Illuminate\Http\Resources\Json\JsonResource;

final class AccountTransactionResource extends JsonResource
{
    public function toArray($request): array
    {
        return [
            'data' => [
                'id' => $this->id,
                'amount' => (float) ($this->amount / 100),
                'created_at' => $this->created_at,
            ]
        ];
    }
}
