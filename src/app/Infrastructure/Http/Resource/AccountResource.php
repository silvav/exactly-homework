<?php

declare(strict_types=1);

namespace App\Infrastructure\Http\Resource;

use Illuminate\Http\Resources\Json\JsonResource;

final class AccountResource extends JsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'balance' => (float) ($this->balance / 100),
            'created_at' => $this->created_at,
        ];
    }
}
