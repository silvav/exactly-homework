<?php

declare(strict_types=1);

namespace App\Infrastructure\Http\Resource;

use App\Infrastructure\Persistence\Mysql\AccountRepository;
use Illuminate\Http\Resources\Json\JsonResource;

final class UserResource extends JsonResource
{
    public function __construct($resource, array $with = [])
    {
        parent::__construct($resource);

        $this->with = $with;
    }

    public function toArray($request): array
    {
        return [
            'data' => [
                'id' => $this->id,
                'name' => $this->name,
                'email' => $this->email,
                'account' => new AccountResource($this->getAccount()),
            ]
        ];
    }
}
