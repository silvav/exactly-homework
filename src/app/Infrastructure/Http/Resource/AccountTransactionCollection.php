<?php

declare(strict_types=1);

namespace App\Infrastructure\Http\Resource;

use App\Domain\Entity\AccountTransaction;
use Illuminate\Http\Resources\Json\ResourceCollection;

final class AccountTransactionCollection extends ResourceCollection
{
    public function toArray($request)
    {
        $this->collection->transform(function (AccountTransaction $accountTransaction) {
            return (new AccountTransactionResource($accountTransaction));
        });
        return parent::toArray($request);
    }
}
