<?php

declare(strict_types=1);

namespace App\Application\CommandHandler;

use App\Application\Command\RegisterUser;
use App\Domain\Entity\Account;
use App\Domain\Entity\Subscription;
use App\Domain\Repository\UserRepository;
use App\Domain\ValueObject\Subscription\RegularSubscription;

final class RegisterUserHandler
{
    private UserRepository $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function handle(RegisterUser $command): void
    {
        $user = $command->getUser();
        $this->userRepository->save($user);

        $account = new Account();
        $user->account()->save($account);

        $subscription = new Subscription((new RegularSubscription())->toArray());
        $user->subscription()->save($subscription);
    }
}
