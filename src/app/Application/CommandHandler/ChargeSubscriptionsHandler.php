<?php

declare(strict_types=1);

namespace App\Application\CommandHandler;

use App\Application\Command\ChargeSubscriptions;
use App\Application\Command\TransferFunds;
use App\Domain\Entity\Account;
use App\Domain\Entity\Subscription;
use App\Domain\Exceptions\InsufficientAccountFunds;
use App\Domain\Repository\SubscriptionRepository;
use Psr\Log\LoggerInterface;

final class ChargeSubscriptionsHandler
{
    private SubscriptionRepository $subscriptionRepository;

    private LoggerInterface $logger;

    public function __construct(
        SubscriptionRepository $subscriptionRepository,
        LoggerInterface $logger
    ) {
        $this->subscriptionRepository = $subscriptionRepository;
        $this->logger = $logger;
    }

    public function handle(ChargeSubscriptions $command)
    {
        /** @var iterable<Subscription> $subscriptions */
        $subscriptions = $this->subscriptionRepository->findChargeable();
        foreach ($subscriptions as $subscription) {
            $account = $subscription->getUser()->getAccount();
            $this->chargeSubscription($account, $subscription);
            $subscription->scheduleNextCharge()->save();
        }
    }

    private function chargeSubscription(Account $account, Subscription $subscription): void
    {
        try {
            TransferFunds::dispatchNow(
                $account,
                [
                    [
                        'account_id' => 1,
                        'amount' => $subscription->cost,
                    ],
                ]
            );
        } catch (InsufficientAccountFunds $exception) {
            $this->logger->error(
                'Subscription charge failed',
                ['exception' => $exception->getMessage()]
            );
        }
    }
}
