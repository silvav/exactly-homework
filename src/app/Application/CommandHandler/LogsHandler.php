<?php

declare(strict_types=1);

namespace App\Application\CommandHandler;

use Closure;
use Psr\Log\LoggerInterface;

final class LogsHandler
{
    protected $logger;

    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    public function handle($command, Closure $next)
    {
        $result = $next($command);

        $this->logger->debug('Command handled: ' . get_class($command));

        return $result;
    }
}
