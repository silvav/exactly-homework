<?php

declare(strict_types=1);

namespace App\Application\CommandHandler;

use Closure;
use Illuminate\Support\Facades\DB;

final class TransactionsHandler
{
    public function handle($command, Closure $next)
    {
        return DB::transaction(static function () use ($command, $next) {
            return $next($command);
        });
    }
}
