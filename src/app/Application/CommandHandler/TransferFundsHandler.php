<?php

declare(strict_types=1);

namespace App\Application\CommandHandler;

use App\Application\Command\TransferFunds;
use App\Domain\Entity\Account;
use App\Domain\Entity\AccountTransaction;
use App\Domain\Exceptions\InsufficientAccountFunds;
use App\Domain\Repository\AccountRepository;
use App\Domain\Repository\AccountTransactionRepository;

final class TransferFundsHandler
{
    private array $accountCredits = [];

    private array $transactions = [];

    private AccountRepository $accountRepository;

    private AccountTransactionRepository $accountTransactionRepository;

    public function __construct(
        AccountRepository $accountRepository,
        AccountTransactionRepository $accountTransactionRepository
    ) {
        $this->accountRepository = $accountRepository;
        $this->accountTransactionRepository = $accountTransactionRepository;
    }

    public function handle(TransferFunds $command): void
    {
        $payerAccount = $command->getPayerAccount();
        $payerAccountId = $payerAccount->getKey();
        $transfers = $command->getTransfers();

        $this->transactions = [];
        $this->accountCredits = [];
        $payerTotalDebitAmount = 0;

        foreach ($transfers as $transfer) {
            $this->collectAccountTransactions($payerAccountId, $transfer);

            $this->collectBeneficiaryCredit($transfer);

            $payerTotalDebitAmount += $this->getAmmount($transfer);
        }

        $this->checkPayerBalance($payerAccount, $payerTotalDebitAmount);

        $this->updatePayerBalance($payerAccountId, $payerTotalDebitAmount);

        $this->accountTransactionRepository->bulkInsert($this->transactions);

        $this->updateBeneficiaryBalances();
    }

    private function updateBeneficiaryBalances(): void
    {
        foreach ($this->accountCredits as $accountId => $creditAmount) {
            $beneficiaryAccount = $this->accountRepository->getByIdWithLock($accountId);
            $beneficiaryAccount->credit($creditAmount);
            $beneficiaryAccount->save();
        }
    }

    private function updatePayerBalance(int $payerAccountId, $debitAmount): void
    {
        $payerAccount = $this->accountRepository->getByIdWithLock($payerAccountId);
        $payerAccount->debit($debitAmount);
        $payerAccount->save();
    }

    private function collectBeneficiaryCredit(array $transfer): void
    {
        if (isset($this->accountCredits[$transfer['account_id']])) {
            $this->accountCredits[$transfer['account_id']] += $this->getAmmount($transfer);
            return;
        }

        $this->accountCredits[$transfer['account_id']] = $this->getAmmount($transfer);
    }

    private function getAmmount(array $transfer): int
    {
        return (int) $transfer['amount'];
    }

    private function collectAccountTransactions(int $payerAccountId, array $transfer): void
    {
        $amount = $this->getAmmount($transfer);
        $this->transactions[] = new AccountTransaction(
            [
                'account_id' => $payerAccountId,
                'amount' => -1 * $amount,
            ]
        );
        $this->transactions[] = new AccountTransaction(
            [
                'account_id' => $transfer['account_id'],
                'amount' => $amount,
            ]
        );
    }

    private function checkPayerBalance(Account $payerAccount, int $payerTotalDebitAmount): void
    {
        $balance = $payerAccount->getBalance();
        if ($balance < $payerTotalDebitAmount) {
            $message = sprintf(
                'Insufficient funds to complete transaction: %.2f',
                ($balance - $payerTotalDebitAmount) / 100
            );
            throw new InsufficientAccountFunds($message);
        }
    }
}
