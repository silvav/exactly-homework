<?php

declare(strict_types=1);

namespace App\Application\Command;

use Illuminate\Foundation\Bus\Dispatchable;

final class ChargeSubscriptions
{
    use Dispatchable;
}
