<?php

declare(strict_types=1);

namespace App\Application\Command;

use App\Domain\Entity\User;
use Illuminate\Foundation\Bus\Dispatchable;

final class RegisterUser
{
    use Dispatchable;

    private User $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    public function getUser(): User
    {
        return $this->user;
    }
}
