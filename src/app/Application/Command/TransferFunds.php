<?php

declare(strict_types=1);

namespace App\Application\Command;

use App\Domain\Entity\Account;
use Illuminate\Foundation\Bus\Dispatchable;

final class TransferFunds
{
    use Dispatchable;

    private Account $payerAccount;

    private array $transfers;

    public function __construct(Account $payerAccount, array $transfers)
    {
        $this->payerAccount = $payerAccount;
        $this->transfers = $transfers;
    }

    public function getPayerAccount(): Account
    {
        return $this->payerAccount;
    }

    public function getTransfers(): array
    {
        return $this->transfers;
    }
}
