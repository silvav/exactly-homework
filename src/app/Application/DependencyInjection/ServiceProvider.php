<?php

declare(strict_types=1);

namespace App\Application\DependencyInjection;

use App\Application\Command\ChargeSubscriptions;
use App\Application\Command\RegisterUser;
use App\Application\Command\TransferFunds;
use App\Application\CommandHandler;
use App\Domain\Repository;
use App\Infrastructure\Persistence\Mysql;
use Illuminate\Support\Facades\Bus;
use Illuminate\Support\ServiceProvider as BaseServiceProvider;

final class ServiceProvider extends BaseServiceProvider
{
    public function boot()
    {
        Bus::pipeThrough([
            CommandHandler\LogsHandler::class,
            CommandHandler\TransactionsHandler::class,
        ]);

        $this->registerCommandHandlers();
    }

    public function register(): void
    {
        $this->app->bind(
            Repository\UserRepository::class,
            Mysql\UserRepository::class
        );

        $this->app->bind(
            Repository\AccountRepository::class,
            Mysql\AccountRepository::class
        );

        $this->app->bind(
            Repository\AccountTransactionRepository::class,
            Mysql\AccountTransactionRepository::class
        );

        $this->app->bind(
            Repository\SubscriptionRepository::class,
            Mysql\SubscriptionRepository::class
        );
    }

    private function registerCommandHandlers()
    {
        Bus::map([
            RegisterUser::class => CommandHandler\RegisterUserHandler::class,
            TransferFunds::class => CommandHandler\TransferFundsHandler::class,
            ChargeSubscriptions::class => CommandHandler\ChargeSubscriptionsHandler::class,
        ]);
    }
}
