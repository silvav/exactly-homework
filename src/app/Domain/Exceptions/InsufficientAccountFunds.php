<?php

declare(strict_types=1);

namespace App\Domain\Exceptions;

use RuntimeException;

class InsufficientAccountFunds extends RuntimeException
{
}
