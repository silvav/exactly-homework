<?php

declare(strict_types=1);

namespace App\Domain\ValueObject\Subscription;

use DateTimeImmutable;

final class RegularSubscription
{
    private const COST = 500;
    private const TYPE = 'regular';
    private const CHARGE_FREQUENCY = '1 month';

    private DateTimeImmutable $chargeAt;

    public function __construct()
    {
        $this->chargeAt = (new DateTimeImmutable())
            ->modify('+ ' . self::CHARGE_FREQUENCY);
    }

    public function toArray(): array
    {
        return [
            'cost' => self::COST,
            'type' => self::TYPE,
            'charge_frequency' => self::CHARGE_FREQUENCY,
            'charge_at' => $this->chargeAt,
        ];
    }
}
