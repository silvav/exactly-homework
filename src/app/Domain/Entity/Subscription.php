<?php

declare(strict_types=1);

namespace App\Domain\Entity;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Subscription extends Model
{
    protected $fillable = [
        'user_id',
        'cost',
        'type',
        'charge_frequency',
        'charge_at',
    ];

    protected $dates = [
        'charge_at',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function getUser(): User
    {
        return $this->user()->get()->first();
    }

    public function scheduleNextCharge(): Subscription
    {
        $chargeAt = $this->getAttribute('charge_at');
        $chargeFrequency = $this->attributes['charge_frequency'];
        $nextChargeAt = $chargeAt->modify('+ ' . $chargeFrequency);
        $this->attributes['charge_at'] = $nextChargeAt;

        return $this;
    }

    public function getChargeAt(): Carbon
    {
        return $this->getAttribute('charge_at');
    }
}
