<?php

declare(strict_types=1);

namespace App\Domain\Entity;

use Illuminate\Database\Eloquent\Model;

class Account extends Model
{
    protected $fillable = [
        'id',
        'user_id',
    ];

    protected $hidden = [
        'user_id',
        'updated_at',
    ];

    public function getTransactions()
    {
        return $this->hasMany(AccountTransaction::class)->cursor();
    }

    public function getBalance(): int
    {
        return $this->getAttribute('balance');
    }

    public function debit(int $amount): Account
    {
        $projectedBalance = $this->getAttribute('balance') - $amount;
        $this->setAttribute('balance', $projectedBalance);

        return $this;
    }

    public function credit(int $amount): Account
    {
        $projectedBalance = $this->getAttribute('balance') + $amount;
        $this->setAttribute('balance', $projectedBalance);

        return $this;
    }
}
