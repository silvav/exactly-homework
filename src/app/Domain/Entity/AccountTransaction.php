<?php

declare(strict_types=1);

namespace App\Domain\Entity;

use Illuminate\Database\Eloquent\Model;

class AccountTransaction extends Model
{
    protected $fillable = [
        'account_id',
        'amount',
    ];

    public function getAmount(): int
    {
        return $this->getAttribute('amount');
    }
}
