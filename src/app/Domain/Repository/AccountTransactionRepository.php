<?php

declare(strict_types=1);

namespace App\Domain\Repository;

interface AccountTransactionRepository
{
    public function findByAccountId(int $accountId);

    public function bulkInsert(array $accountTransactions);
}
