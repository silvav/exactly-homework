<?php

declare(strict_types=1);

namespace App\Domain\Repository;

use App\Domain\Entity\Subscription;

interface SubscriptionRepository
{
    /**
     * @return iterable<Subscription>
     */
    public function findChargeable(): iterable;

    public function findAll();

    public function get(int $id): Subscription;
}
