<?php

declare(strict_types=1);

namespace App\Domain\Repository;

use App\Domain\Entity\Account;

interface AccountRepository
{
    public function getByIdWithLock($id): Account;

    public function get(int $id): Account;
}
