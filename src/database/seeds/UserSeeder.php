<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    public function run(): void
    {
        $now = new DateTime();
        $password = Hash::make('secret');

        DB::table('users')
            ->insert(
                [
                    [
                        'id' => 1,
                        'name' => 'ACME LTD.',
                        'email' => 'admin@acme.com',
                        'password' => $password,
                        'created_at' => $now,
                        'updated_at' => $now,
                    ],
                    [
                        'id' => 2,
                        'name' => 'Foo Bar',
                        'email' => 'foo@bar.com',
                        'password' => $password,
                        'created_at' => $now,
                        'updated_at' => $now,
                    ],
                    [
                        'id' => 3,
                        'name' => 'John Smith',
                        'email' => 'john@smith.com',
                        'password' => $password,
                        'created_at' => $now,
                        'updated_at' => $now,
                    ],
                    [
                        'id' => 4,
                        'name' => 'Jane Doe',
                        'email' => 'jane@doe.com',
                        'password' => $password,
                        'created_at' => $now,
                        'updated_at' => $now,
                    ],
                    [
                        'id' => 5,
                        'name' => 'Joe Doe',
                        'email' => 'joe@doe.com',
                        'password' => $password,
                        'created_at' => $now,
                        'updated_at' => $now,
                    ],
                ]
            );
    }
}
