<?php

use Illuminate\Database\Seeder;

class AccountSeeder extends Seeder
{
    public function run(): void
    {
        $now = new DateTime();

        DB::table('accounts')
            ->insert(
                [
                    [
                        'id' => 1,
                        'user_id' => 1,
                        'balance' => 50000,
                        'created_at' => $now,
                        'updated_at' => $now,
                    ],
                    [
                        'id' => 2,
                        'user_id' => 2,
                        'balance' => 50049,
                        'created_at' => $now,
                        'updated_at' => $now,
                    ],
                    [
                        'id' => 3,
                        'user_id' => 3,
                        'balance' => 50000,
                        'created_at' => $now,
                        'updated_at' => $now,
                    ],
                    [
                        'id' => 4,
                        'user_id' => 4,
                        'balance' => 50000,
                        'created_at' => $now,
                        'updated_at' => $now,
                    ],
                    [
                        'id' => 5,
                        'user_id' => 5,
                        'balance' => 70064,
                        'created_at' => $now,
                        'updated_at' => $now,
                    ],
                ]
            );
    }
}
