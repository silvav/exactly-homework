<?php

use Illuminate\Database\Seeder;

class SubscriptionSeeder extends Seeder
{
    public function run(): void
    {
        $now = new DateTime();

        $tomorrow = (new \DateTimeImmutable())->modify('+1 day');

        DB::table('subscriptions')
            ->insert(
                [
                    [
                        'id' => 1,
                        'user_id' => 1,
                        'cost' => 500,
                        'type' => 'regular',
                        'charge_frequency' => '',
                        'charge_at' => null,
                        'created_at' => $now,
                        'updated_at' => $now,
                    ],
                    [
                        'id' => 2,
                        'user_id' => 2,
                        'cost' => 500,
                        'type' => 'regular',
                        'charge_frequency' => '1 month',
                        'charge_at' => new DateTime('2020-03-15 10:34:00'),
                        'created_at' => $now,
                        'updated_at' => $now,
                    ],
                    [
                        'id' => 3,
                        'user_id' => 3,
                        'cost' => 1500,
                        'type' => 'gold',
                        'charge_frequency' => '1 month',
                        'charge_at' => $tomorrow,
                        'created_at' => $now,
                        'updated_at' => $now,
                    ],
                    [
                        'id' => 4,
                        'user_id' => 4,
                        'cost' => 1500,
                        'type' => 'gold',
                        'charge_frequency' => '1 month',
                        'charge_at' => $tomorrow,
                        'created_at' => $now,
                        'updated_at' => $now,
                    ],
                    [
                        'id' => 5,
                        'user_id' => 5,
                        'cost' => 1500,
                        'type' => 'gold',
                        'charge_frequency' => '1 month',
                        'charge_at' => $tomorrow,
                        'created_at' => $now,
                        'updated_at' => $now,
                    ],
                ]
            );
    }
}
