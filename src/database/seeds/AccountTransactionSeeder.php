<?php

declare(strict_types=1);

use Illuminate\Database\Seeder;

class AccountTransactionSeeder extends Seeder
{
    public function run(): void
    {
        $now = new DateTime();

        DB::table('account_transactions')
            ->insert(
                [
                    [
                        'id' => 1,
                        'account_id' => 1,
                        'amount' => 50000,
                        'created_at' => $now,
                        'updated_at' => $now,
                    ],
                    [
                        'id' => 2,
                        'account_id' => 2,
                        'amount' => 50049,
                        'created_at' => $now,
                        'updated_at' => $now,
                    ],
                    [
                        'id' => 3,
                        'account_id' => 3,
                        'amount' => 50000,
                        'created_at' => $now,
                        'updated_at' => $now,
                    ],
                    [
                        'id' => 4,
                        'account_id' => 4,
                        'amount' => 50000,
                        'created_at' => $now,
                        'updated_at' => $now,
                    ],
                    [
                        'id' => 5,
                        'account_id' => 5,
                        'amount' => 5011,
                        'created_at' => $now,
                        'updated_at' => $now,
                    ],
                    [
                        'id' => 6,
                        'account_id' => 5,
                        'amount' => 5012,
                        'created_at' => $now,
                        'updated_at' => $now,
                    ],
                    [
                        'id' => 7,
                        'account_id' => 5,
                        'amount' => 5013,
                        'created_at' => $now,
                        'updated_at' => $now,
                    ],
                    [
                        'id' => 8,
                        'account_id' => 5,
                        'amount' => 5014,
                        'created_at' => $now,
                        'updated_at' => $now,
                    ],
                    [
                        'id' => 9,
                        'account_id' => 5,
                        'amount' => 5015,
                        'created_at' => $now,
                        'updated_at' => $now,
                    ],
                    [
                        'id' => 10,
                        'account_id' => 5,
                        'amount' => 5000,
                        'created_at' => $now,
                        'updated_at' => $now,
                    ],
                    [
                        'id' => 11,
                        'account_id' => 5,
                        'amount' => 5000,
                        'created_at' => $now,
                        'updated_at' => $now,
                    ],
                    [
                        'id' => 12,
                        'account_id' => 5,
                        'amount' => 5000,
                        'created_at' => $now,
                        'updated_at' => $now,
                    ],
                    [
                        'id' => 13,
                        'account_id' => 5,
                        'amount' => 5000,
                        'created_at' => $now,
                        'updated_at' => $now,
                    ],
                    [
                        'id' => 14,
                        'account_id' => 5,
                        'amount' => 5000,
                        'created_at' => $now,
                        'updated_at' => $now,
                    ],
                    [
                        'id' => 15,
                        'account_id' => 5,
                        'amount' => 5000,
                        'created_at' => $now,
                        'updated_at' => $now,
                    ],
                    [
                        'id' => 16,
                        'account_id' => 5,
                        'amount' => 5000,
                        'created_at' => $now,
                        'updated_at' => $now,
                    ],
                    [
                        'id' => 17,
                        'account_id' => 5,
                        'amount' => 5000,
                        'created_at' => $now,
                        'updated_at' => $now,
                    ],
                    [
                        'id' => 18,
                        'account_id' => 5,
                        'amount' => 5000,
                        'created_at' => $now,
                        'updated_at' => $now,
                    ],
                ]
            );
    }
}
