<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            UserSeeder::class,
            AccountSeeder::class,
            SubscriptionSeeder::class,
            AccountTransactionSeeder::class,
        ]);
    }
}
