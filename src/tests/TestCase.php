<?php

namespace App;

use App\Domain\Entity\User;
use App\Domain\Repository\UserRepository;
use Illuminate\Contracts\Console\Kernel;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;

abstract class TestCase extends BaseTestCase
{
    /**
     * The base URL to use while testing the application.
     *
     * @var string
     */
    protected $baseUrl = 'http://localhost';

    protected function setUp(): void
    {
        parent::setUp();

        $database = env('DB_DATABASE');
        $this->setUpDatabase($database);
        //touch('./tests.sqlite');
        //Artisan::call('db:seed');
    }

    public function tearDown(): void
    {
        //unlink('./tests.sqlite');
    }

    /**
     * Creates the application.
     *
     * @return \Illuminate\Foundation\Application
     */
    public function createApplication()
    {
        $app = require __DIR__ . '/../bootstrap/app.php';

        $app->make(Kernel::class)->bootstrap();

        return $app;
    }

    private function setUpDatabase(string $database): void
    {
        $this->recreateMySQLDatabase($database);
        $this->artisan('migrate', ['--verbose' => 2]);
    }

    private function recreateMySQLDatabase(string $database): void
    {
        $this->setConnectionDatabase('mysql', null);

        $this->dropAndCreate($database);

        $this->reconnectDatabase('mysql', $database);
    }

    private function setConnectionDatabase(string $connection, ?string $database): void
    {
        $key = sprintf('database.connections.%s.database', $connection);
        Config::set($key, $database);
    }

    private function dropAndCreate(string $database): void
    {
        DB::statement("DROP DATABASE IF EXISTS $database;");
        DB::statement("CREATE DATABASE $database;");
    }

    private function reconnectDatabase(string $connection, string $database): void
    {
        $this->setConnectionDatabase($connection, $database);
        DB::purge($connection);
        DB::reconnect($connection);
    }

    public function createUser(array $userData)
    {
        $requestPayload = [
            'name' => $userData['name'] ?? 'Foo Bar',
            'email' => $userData['email'] ?? 'foo@bar.com',
            'password' => $userData['password'] ?? 'secret',
        ];

        $response = $this->post('api/users', $requestPayload);

        /** @var UserRepository $userRepository */
        $userRepository = $this->app->get(UserRepository::class);
        $userId = (int) $response->json('data')['id'];

        return $userRepository->get($userId);
    }

    public function getUserToken(User $user, string $password): string
    {
        $uri = sprintf('api/users/%s/login', $user->getKey());
        $userData = [
            'email' => $user->email,
            'password' => $password,
        ];
        $response = $this->post($uri, $userData);

        return $response->json('token');
    }

    public function getAuthorizationHeaders(string $token): array
    {
        return [
            'authorization' => 'Bearer ' . $token,
        ];
    }
}
