<?php

declare(strict_types=1);

namespace App\Functional\Infrastructure\Console\ConsoleCommand;

use App\Domain\Repository\AccountRepository;
use App\Domain\Repository\SubscriptionRepository;
use App\TestCase;
use Illuminate\Support\Facades\Artisan;

/**
 * @property SubscriptionRepository subscriptionRepository
 * @property AccountRepository accountRepository
 */
class ChargeSubscriptionConsoleCommandTest extends TestCase
{
    protected function setUp(): void
    {
        parent::setUp();
        Artisan::call('db:seed');
        $this->subscriptionRepository = $this->app->get(SubscriptionRepository::class);
        $this->accountRepository = $this->app->get(AccountRepository::class);
    }

    public function testSubscriptionChargeShouldScheduleNextCharge(): void
    {
        // When
        $exitCode = Artisan::call('charge:subscriptions');

        // Then
        $this->assertSame(0, $exitCode);

        $subscription = $this->subscriptionRepository->get(2);
        $chargeAt = $subscription->getChargeAt()->format('Y-m-d');
        $this->assertSame('2020-04-15', $chargeAt);
    }

    public function testSubscriptionChargeShouldNotScheduleNextCharge(): void
    {
        // Given
        $subscription3 = $this->subscriptionRepository->get(3);
        $subscription3chargeAt = $subscription3->getChargeAt()->format('Y-m-d');

        // When
        $exitCode = Artisan::call('charge:subscriptions');

        // Then
        $this->assertSame(0, $exitCode);

        $subscription = $this->subscriptionRepository->get(3);
        $chargeAt = $subscription->getChargeAt()->format('Y-m-d');
        $this->assertSame($subscription3chargeAt, $chargeAt);
    }

    public function testSubscriptionChargeShouldDebitCostFromAccount(): void
    {
        // When
        $exitCode = Artisan::call('charge:subscriptions');

        // Then
        $this->assertSame(0, $exitCode);

        $serviceAccount = $this->accountRepository->get(1);
        $this->assertSame(50500, $serviceAccount->getBalance());

        $userAccount = $this->accountRepository->get(2);
        $this->assertSame(49549, $userAccount->getBalance());
    }

    public function testSubscriptionChargeShouldNotDebitCostFromAccount(): void
    {
        // When
        $exitCode = Artisan::call('charge:subscriptions');

        // Then
        $this->assertSame(0, $exitCode);

        $userAccount = $this->accountRepository->get(3);
        $this->assertSame(50000, $userAccount->getBalance());
    }
}
