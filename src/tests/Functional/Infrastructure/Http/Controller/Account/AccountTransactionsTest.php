<?php

declare(strict_types=1);

namespace App\Functional\Infrastructure\Http\Controller\Account;

use App\Domain\Repository\AccountTransactionRepository;
use App\Domain\Repository\UserRepository;
use App\TestCase;
use Illuminate\Support\Facades\Artisan;

/**
 * @property string token
 */
class AccountTransactionsTest extends TestCase
{
    protected function setUp(): void
    {
        parent::setUp();
        Artisan::call('db:seed');
        $this->accountTransactionRepository = $this->app->get(AccountTransactionRepository::class);
        $userRepository = $this->app->get(UserRepository::class);
        $this->token = $this->getUserToken($userRepository->get(1), 'secret');
    }

    public function testAccountTransferToOneAccountShouldSucceed(): void
    {
        // When
        $response = $this->get(
            'api/users/5/accounts/5/transactions',
            $this->getAuthorizationHeaders($this->token)
        );

        // Then
        $response->assertStatus(200);
        $response->assertJsonCount(5, 'data');

        $response = $this->get(
            'api/users/5/accounts/5/transactions?page=3',
            $this->getAuthorizationHeaders($this->token)
        );
        $response->assertStatus(200);
        $response->assertJsonCount(4, 'data');
    }
}
