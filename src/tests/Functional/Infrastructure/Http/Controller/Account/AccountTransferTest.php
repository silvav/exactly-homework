<?php

declare(strict_types=1);

namespace App\Functional\Infrastructure\Http\Controller\Account;

use App\Domain\Repository\AccountRepository;
use App\Domain\Repository\AccountTransactionRepository;
use App\Domain\Repository\UserRepository;
use App\TestCase;
use Illuminate\Support\Facades\Artisan;

/**
 * @property AccountTransactionRepository accountTransactionRepository
 * @property AccountRepository accountRepository
 * @property string token
 */
class AccountTransferTest extends TestCase
{
    protected function setUp(): void
    {
        parent::setUp();
        $this->accountTransactionRepository = $this->app->get(AccountTransactionRepository::class);
        $this->accountRepository = $this->app->get(AccountRepository::class);
        $userRepository = $this->app->get(UserRepository::class);
        Artisan::call('db:seed');
        $this->token = $this->getUserToken($userRepository->get(1), 'secret');
    }

    public function testAccountTransferToOneAccountShouldSucceed(): void
    {
        // Given
        $transferData = [
            'transfers' => [
                [
                    'account_id' => 2,
                    'amount' => 50.00,
                ],
            ]
        ];
        $account1Balance = $this->accountRepository->get(1)->getBalance();

        // When
        $response = $this->post(
            'api/users/1/accounts/1/transfer',
            $transferData,
            $this->getAuthorizationHeaders($this->token)
        );

        // Then
        $response->assertStatus(200);

        $account1Balance = $this->accountRepository->get(1)->getBalance();
        $this->assertSame(45000, $account1Balance);
        $transactions = $this->accountTransactionRepository->findByAccountId(1);
        $this->assertCount(2, $transactions);
        $this->assertSame(-5000, $transactions[1]->getAmount());

        $actualBalance = $this->accountRepository->get(2)->getBalance();
        $this->assertSame(55049, $actualBalance);
        $transactions2 = $this->accountTransactionRepository->findByAccountId(2);

        $this->assertCount(2, $transactions2);
        $this->assertSame(5000, $transactions2[1]->getAmount());
    }

    public function testAccountTransferToMultipleAccountsShouldSucceed(): void
    {
        // Given
        $transferData = [
            'transfers' => [
                [
                    'account_id' => 2,
                    'amount' => 50.00,
                ],
                [
                    'account_id' => 3,
                    'amount' => 50.01,
                ],
                [
                    'account_id' => 4,
                    'amount' => 250.41,
                ],
            ]
        ];

        // When
        $response = $this->post(
            'api/users/1/accounts/1/transfer',
            $transferData,
            $this->getAuthorizationHeaders($this->token)
        );

        // Then
        $response->assertStatus(200);

        $actualBalance = $this->accountRepository->get(1)->getBalance();
        $this->assertSame(14958, $actualBalance);
        $transactions = $this->accountTransactionRepository->findByAccountId(1);
        $this->assertCount(4, $transactions);
        $this->assertSame(-5000, $transactions[1]->getAmount());
        $this->assertSame(-5001, $transactions[2]->getAmount());
        $this->assertSame(-25041, $transactions[3]->getAmount());

        $actualBalance = $this->accountRepository->get(2)->getBalance();
        $this->assertSame(55049, $actualBalance);
        $transactions2 = $this->accountTransactionRepository->findByAccountId(2);
        $this->assertCount(2, $transactions2);
        $this->assertSame(5000, $transactions2[1]->getAmount());

        $actualBalance = $this->accountRepository->get(3)->getBalance();
        $this->assertSame(55001, $actualBalance);
        $transactions3 = $this->accountTransactionRepository->findByAccountId(3);
        $this->assertCount(2, $transactions3);
        $this->assertSame(5001, $transactions3[1]->getAmount());
    }
}
