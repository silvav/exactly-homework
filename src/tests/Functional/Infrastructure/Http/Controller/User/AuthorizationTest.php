<?php

declare(strict_types=1);

namespace App\Functional\Infrastructure\Http\Controller\User;

use App\TestCase;

class AuthorizationTest extends TestCase
{
    public function testUnAuthenticatedUserAccessShouldFail(): void
    {
        // Given
        $password = '123456';
        $userData = [
            'email' => 'test@email.com',
            'password' => $password
        ];
        $user = $this->createUser($userData);

        // When
        $response = $this->get('api/users/1');

        // Then
        $response->assertStatus(401);
    }

    public function testUserAuthenticationShouldSucceed(): void
    {
        // Given
        $password = '123456';
        $userData = [
            'email' => 'test@email.com',
            'password' => $password
        ];
        $user = $this->createUser($userData);
        $token = $this->getUserToken($user, $password);

        // When
        $response = $this->get(
            'api/users/1',
            $this->getAuthorizationHeaders($token)
        );

        // Then
        $response->assertStatus(200);
    }
}
