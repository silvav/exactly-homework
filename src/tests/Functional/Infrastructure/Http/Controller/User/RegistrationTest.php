<?php

declare(strict_types=1);

namespace App\Functional\Infrastructure\Http\Controller\User;

use App\Domain\Repository\UserRepository;
use App\TestCase;
use Illuminate\Foundation\Testing\TestResponse;

class RegistrationTest extends TestCase
{
    public function testUserRegistrationShouldSucceed(): void
    {
        // Given
        /** @var UserRepository $userRepository */
        $userRepository = $this->app->get(UserRepository::class);

        $name = 'Test User';
        $email = 'test@email.com';

        // When
        /** @var TestResponse $response */
        $response = $this->post('api/users', [
            'name' => $name,
            'email' => $email,
            'password' => '123456'
        ]);

        // Then
        $response->assertStatus(201);

        $userId = (int) $response->json('data')['id'];
        $user = $userRepository->get($userId);
        $response->assertJsonStructure(
            [
                'data' => [
                    'id',
                    'name',
                    'email',
                    'account' => [
                        'balance',
                        'created_at',
                        'id',
                    ],
                ],
            ]
        );
        $this->assertSame($name, $user->name);
        $this->assertSame($email, $user->email);
    }

    public function testDuplicateUserRegistrationShouldFail(): void
    {
        // Given
        $userData = [
            'name' => 'Test User',
            'email' => 'test@email.com',
            'password' => '123456'
        ];

        // When
        $firstResponse = $this->post('api/users', $userData);
        $secondResponse = $this->post('api/users', $userData);

        // Then
        $firstResponse->assertStatus(201);
        $secondResponse->assertStatus(409);
    }

    /**
     * @dataProvider provideInvalidRequestData
     * @param array $userData
     */
    public function testInvalidRequestUserRegistrationShouldFail(array $userData): void
    {
        // When
        $firstResponse = $this->post('api/users', $userData);

        // Then
        $firstResponse->assertStatus(422);
    }

    public function provideInvalidRequestData(): array
    {
        return [
            'missing password' => [
                [
                    'name' => 'Test User',
                    'email' => 'test@email.com',
                ],
            ],
            'missing name' => [
                [
                    'password' => 'secret',
                    'email' => 'test@email.com',
                ],
            ],
            'missing email' => [
                [
                    'name' => 'Test User',
                    'password' => 'secret',
                ],
            ],
            'invalid email' => [
                [
                    'name' => 'Test User',
                    'password' => 'secret',
                    'email' => 'testemail.com',
                ],
            ],
        ];
    }
}
