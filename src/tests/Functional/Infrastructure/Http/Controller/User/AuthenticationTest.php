<?php

declare(strict_types=1);

namespace App\Functional\Infrastructure\Http\Controller\User;

use App\TestCase;

class AuthenticationTest extends TestCase
{
    public function testUserAuthenticationShouldSucceed(): void
    {
        // Given
        $userData = [
            'email' => 'test@email.com',
            'password' => '123456'
        ];
        $user = $this->createUser($userData);

        // When
        $response = $this->post('api/users/1/login', $userData);

        // Then
        $response->assertStatus(200);
        $response->assertJsonStructure(['data', 'token', 'expires_in']);
        $this->assertNotEmpty($response->json('token'));
    }

    public function testWrongUserPasswordAuthenticationShouldFail(): void
    {
        // Given
        $userData = [
            'email' => 'test@email.com',
            'password' => 'secret'
        ];
        $this->createUser($userData);

        // When
        $response = $this->post(
            'api/users/1/login',
            [
                'email' => 'test@email.com',
                'password' => 'wrong_password'
            ]
        );

        // Then
        $response->assertStatus(403);
    }

    public function testUserDoesNotExistAuthenticationShouldFail(): void
    {
        // Given
        $userData = [
            'email' => 'test@email.com',
            'password' => '123456'
        ];

        // When
        $firstResponse = $this->post('api/users/1/login', $userData);

        // Then
        $firstResponse->assertStatus(403);
    }
}
