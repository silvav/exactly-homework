# TransFund API

## Requirements

The API must:

- ✅ Use Laravel framework;

- ✅ Register and authorize users based on tokens;

- ✅ Provide a method of sending funds to another user of the system;

- ✅ Provide a method by which you can send funds to multiple users of the system;

- ✅ Provide a method for obtaining a list of financial transactions in a user account;

- ✅ Scheduled automatic deduction of funds from the user's account (subscription fee);

- ✅ It is imperative to take into account cases of race condition when transactions can be executed “simultaneously.”;

- ✅ Write tests for the main endpoints;

- ✅ Code style conforms to PSR-12.

## Installation

### Runtime Requirements
Make sure you have [Docker](https://www.docker.com/) and [Docker Compose](https://docs.docker.com/compose/) installed.

Ports 80 and 3306 are used by the web and database servers.

The web server can be accessed via http://localhost 

### Installation guide

It can take a while for the environment to become ready.

In case any issues, such as port conflict with existing running containers, try restarting your Docker Desktop. 

```
git clone https://silvav@bitbucket.org/silvav/exactly-homework.git homework && cd homework && make
```

## Testing Notes

To ease the testing of the API data is seeded into the database.
The [Makefile](Makefile) contains rules to ease testing and further development of the API.

Should the data need to be reset, it can be done by running the following command on the shell:

```
make reset-data
```

### Automated tests

Run the test suite via the console.

```
make test
```

Output

```
docker-compose up -d
exactly_app is up-to-date
exactly_webserver is up-to-date
exactly_db is up-to-date
docker exec -it exactly_app /bin/bash -c "cd /var/www/src/ && ./vendor/bin/phpunit -c phpunit.xml ./tests"
PHPUnit 7.5.20 by Sebastian Bergmann and contributors.

..................                                                18 / 18 (100%)

Time: 10.04 seconds, Memory: 34.00 MB

OK (18 tests, 58 assertions)

```

### User registration

Request

```
curl --location --request POST 'http://localhost/api/users' \
--header 'Content-Type: application/json' \
--data-raw '{"name":"User Name", "email":"user@mail.com", "password":"secret"}'
```

Response

```json
{
  "data": {
    "id": 6,
    "name": "User Name",
    "email": "user@mail.com",
    "account": {
      "id": 6,
      "balance": 0,
      "created_at": "2020-04-05T01:00:00.000000Z"
    }
  }
}
```

### User authorization

User authorization is done using JWT tokens.
Let's authenticate by issuing a request to the login endpoint to acquire a token.

Request

```
curl --location --request POST 'http://localhost/api/users/2/login' \
--header 'Content-Type: application/json' \
--data-raw '{"email":"foo@bar.com", "password":"secret"}'
```

Response

```json
{
  "data": {
    "id": 2,
    "name": "Foo Bar",
    "email": "foo@bar.com",
    "account": {
      "id": 2,
      "balance": 500.49,
      "created_at": "2020-04-05T00:00:00.000000Z"
    }
  },
  "token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vbG9jYWxob3N0L2FwaS91c2Vycy8yL2xvZ2luIiwiaWF0IjoxNTg2MjQ3MjE4LCJleHAiOjE1ODYyNTA4MTgsIm5iZiI6MTU4NjI0NzIxOCwianRpIjoiMjVCZW1pdW5jUVpuTlQ2TSIsInN1YiI6MiwicHJ2IjoiMDM2OWU5Zjk5ZDJhODliNmNiMDUwOWE4NTVkZWVhNDFkZjJkNzdhMiJ9.8YZa0vkxti4K4y6wE5q2kmCAxbePzQMnmvkaP97MCd8",
  "expires_in": 3600
}
```

This response contains the JWT token used in the next steps for request authorization.

Copy the token value from your response and replace your token value in the next requests. 

In the response above the token is:

```text
eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vbG9jYWxob3N0L2FwaS91c2Vycy8yL2xvZ2luIiwiaWF0IjoxNTg2MjQ3MjE4LCJleHAiOjE1ODYyNTA4MTgsIm5iZiI6MTU4NjI0NzIxOCwianRpIjoiMjVCZW1pdW5jUVpuTlQ2TSIsInN1YiI6MiwicHJ2IjoiMDM2OWU5Zjk5ZDJhODliNmNiMDUwOWE4NTVkZWVhNDFkZjJkNzdhMiJ9.8YZa0vkxti4K4y6wE5q2kmCAxbePzQMnmvkaP97MCd8
```

### Transferring Funds

#### Transferring funds to one account

Request

```
curl --location --request POST 'http://localhost/api/users/2/accounts/2/transfer' \
--header 'Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vbG9jYWxob3N0L2FwaS91c2Vycy8yL2xvZ2luIiwiaWF0IjoxNTg2MjQ3MjE4LCJleHAiOjE1ODYyNTA4MTgsIm5iZiI6MTU4NjI0NzIxOCwianRpIjoiMjVCZW1pdW5jUVpuTlQ2TSIsInN1YiI6MiwicHJ2IjoiMDM2OWU5Zjk5ZDJhODliNmNiMDUwOWE4NTVkZWVhNDFkZjJkNzdhMiJ9.8YZa0vkxti4K4y6wE5q2kmCAxbePzQMnmvkaP97MCd8' \
--header 'Content-Type: application/json' \
--data-raw '{"transfers": [{"account_id": 6, "amount": 50.00}]}'
```

Response

```json
{
  "data": {
    "id": 2,
    "balance": 450.49,
    "created_at": "2020-04-05T00:00:00.000000Z"
  }
}
```


#### Transferring funds to multiple account

Request

```
curl --location --request POST 'http://localhost/api/users/2/accounts/2/transfer' \
--header 'Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vbG9jYWxob3N0L2FwaS91c2Vycy8yL2xvZ2luIiwiaWF0IjoxNTg2MjQ3MjE4LCJleHAiOjE1ODYyNTA4MTgsIm5iZiI6MTU4NjI0NzIxOCwianRpIjoiMjVCZW1pdW5jUVpuTlQ2TSIsInN1YiI6MiwicHJ2IjoiMDM2OWU5Zjk5ZDJhODliNmNiMDUwOWE4NTVkZWVhNDFkZjJkNzdhMiJ9.8YZa0vkxti4K4y6wE5q2kmCAxbePzQMnmvkaP97MCd8' \
--header 'Content-Type: application/json' \
--data-raw '{"transfers": [{"account_id": 4, "amount": 7.99}, {"account_id": 6, "amount": 12.33}]}'
```

Response

```json
{
  "data": {
    "id": 2,
    "balance": 430.17,
    "created_at": "2020-04-05T00:00:00.000000Z"
  }
}
```

### Listing account transactions

Request

```
curl --location --request GET 'http://localhost/api/users/2/accounts/2/transactions' \
--header 'Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vbG9jYWxob3N0L2FwaS91c2Vycy8yL2xvZ2luIiwiaWF0IjoxNTg2MjQ3MjE4LCJleHAiOjE1ODYyNTA4MTgsIm5iZiI6MTU4NjI0NzIxOCwianRpIjoiMjVCZW1pdW5jUVpuTlQ2TSIsInN1YiI6MiwicHJ2IjoiMDM2OWU5Zjk5ZDJhODliNmNiMDUwOWE4NTVkZWVhNDFkZjJkNzdhMiJ9.8YZa0vkxti4K4y6wE5q2kmCAxbePzQMnmvkaP97MCd8' \
--header 'Content-Type: application/json'
```

Response

```json
{
  "data": [
    {
      "data": {
        "id": 21,
        "amount": -7.99,
        "created_at": "2020-04-05T08:29:26.000000Z"
      }
    },
    {
      "data": {
        "id": 23,
        "amount": -12.33,
        "created_at": "2020-04-05T08:29:26.000000Z"
      }
    },
    {
      "data": {
        "id": 19,
        "amount": -50,
        "created_at": "2020-04-05T08:28:09.000000Z"
      }
    },
    {
      "data": {
        "id": 2,
        "amount": 500.49,
        "created_at": "2020-04-05T01:00:00.000000Z"
      }
    }
  ],
  "links": {
    "first": "http://localhost/api/users/2/accounts/2/transactions?page=1",
    "last": "http://localhost/api/users/2/accounts/2/transactions?page=1",
    "prev": null,
    "next": null
  },
  "meta": {
    "current_page": 1,
    "from": 1,
    "last_page": 1,
    "path": "http://localhost/api/users/2/accounts/2/transactions",
    "per_page": 5,
    "to": 4,
    "total": 4
  }
}
```

### Automatic deduction of funds (subscription)

[Subscription charges are scheduled to run daily](./src/app/Infrastructure/Console/Kernel.php)

To test subscription charge let's use the data seeded and manually execute the console command.

Manual console command execution

```
make charge-subscriptions
```

Verifying the expected execution outcome:

1. List account transactions

```
curl --location --request GET 'http://localhost/api/users/2/accounts/2/transactions' \
--header 'Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vbG9jYWxob3N0L2FwaS91c2Vycy8yL2xvZ2luIiwiaWF0IjoxNTg2MjQ3MjE4LCJleHAiOjE1ODYyNTA4MTgsIm5iZiI6MTU4NjI0NzIxOCwianRpIjoiMjVCZW1pdW5jUVpuTlQ2TSIsInN1YiI6MiwicHJ2IjoiMDM2OWU5Zjk5ZDJhODliNmNiMDUwOWE4NTVkZWVhNDFkZjJkNzdhMiJ9.8YZa0vkxti4K4y6wE5q2kmCAxbePzQMnmvkaP97MCd8' \
--header 'Content-Type: application/json'
```

Response

```json
{
    "data": [
        {
            "data": {
                "id": 25,
                "amount": -5,
                "created_at": "2020-04-05T08:44:36.000000Z"
            }
        },
        {
            "data": {
                "id": 21,
                "amount": -7.99,
                "created_at": "2020-04-05T08:29:26.000000Z"
            }
        },
        {
            "data": {
                "id": 23,
                "amount": -12.33,
                "created_at": "2020-04-05T08:29:26.000000Z"
            }
        },
        {
            "data": {
                "id": 19,
                "amount": -50,
                "created_at": "2020-04-05T08:28:09.000000Z"
            }
        },
        {
            "data": {
                "id": 2,
                "amount": 500.49,
                "created_at": "2020-04-05T08:26:17.000000Z"
            }
        }
    ],
    "links": {
        "first": "http://localhost/api/users/2/accounts/2/transactions?page=1",
        "last": "http://localhost/api/users/2/accounts/2/transactions?page=1",
        "prev": null,
        "next": null
    },
    "meta": {
        "current_page": 1,
        "from": 1,
        "last_page": 1,
        "path": "http://localhost/api/users/2/accounts/2/transactions",
        "per_page": 5,
        "to": 5,
        "total": 5
    }
}
```

2. Get account and check balance

```
curl --location --request GET 'http://localhost/api/users/2' \
--header 'Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vbG9jYWxob3N0L2FwaS91c2Vycy8yL2xvZ2luIiwiaWF0IjoxNTg2MjQ3MjE4LCJleHAiOjE1ODYyNTA4MTgsIm5iZiI6MTU4NjI0NzIxOCwianRpIjoiMjVCZW1pdW5jUVpuTlQ2TSIsInN1YiI6MiwicHJ2IjoiMDM2OWU5Zjk5ZDJhODliNmNiMDUwOWE4NTVkZWVhNDFkZjJkNzdhMiJ9.8YZa0vkxti4K4y6wE5q2kmCAxbePzQMnmvkaP97MCd8'
```

Response

```json
{
  "data": {
    "id": 2,
    "name": "Foo Bar",
    "email": "foo@bar.com",
    "account": {
      "id": 2,
      "balance": 425.17,
      "created_at": "2020-04-05T00:00:00.000000Z"
    }
  }
}
```

### Transaction race conditions

[Financial operations are wrapped by database transactions.](./src/app/Application/CommandHandler/TransactionsHandler.php)

[Pessimistic locking ensures integrity of account balance](./src/app/Infrastructure/Persistence/Mysql/AccountRepository.php).

### Code style check

Code style automation

```
make check-style
```

Output

```
docker exec -it exactly_app /bin/bash -c "cd /var/www/src/ && vendor/bin/ecs check . --config ecs.yaml"

                                                                                                                        
 [OK] No errors found. Great job - your code is shiny in style!
```
